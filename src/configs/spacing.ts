import { ScaleSize } from '.';
const Spacing = (number: number) => {
  return number * ScaleSize(4);
};
export default Spacing;
